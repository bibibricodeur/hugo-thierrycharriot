console.log('This site was generated by Hugo 14h24.');

// https://developer.mozilla.org/fr/docs/Web/API/Window/DOMContentLoaded_event
window.addEventListener("DOMContentLoaded", (event) => {
    console.log("DOM entièrement chargé et analysé");

    // https://developer.mozilla.org/fr/docs/Web/API/GlobalEventHandlers/onscroll
    window.onscroll = function () {
        display_button()
    };
    function display_button() {
        // https://developer.mozilla.org/fr/docs/Web/API/Document/getElementById
        let elem = document.getElementById('button-top');
        let numberPixels = window.pageYOffset | document.body.scrollTop;
        if (numberPixels > 360) {
            elem.style.display = 'block';
        } else if (numberPixels <= 360) {
            elem.style.display = 'none';
        }
    }
});
/*
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);
});
*/
// https://materializecss.com/parallax.html
/*
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems, options);
});
*/
// Or with jQuery

$(document).ready(function () {
    $('.parallax').parallax();
    console.log('glop glop 14h20');
});


/*
$(document).ready(function () {
    // Handler for .ready() called.
    $('.parallax').parallax();
    console.log('glop glop');
});
*/